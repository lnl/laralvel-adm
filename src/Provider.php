<?php

namespace Uhuu\Adm;

use Illuminate\Support\ServiceProvider;

class Provider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__. '/Git.php';
        include __DIR__. '/Assets.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            'Uhuu\adm\Git',
            'Uhuu\adm\Assets'
        ]);

    }
}
