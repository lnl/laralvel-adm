<?php

namespace Uhuu\Adm;

use Illuminate\Console\Command;

class Assets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:publicar {--watch}';

    /**
     * The console command description.
     *ra
     * @var string
     */
    protected $description = 'Minifica e gera o uglify dos assets do projeto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
			  $version = env('VERSAO_ASSETS');

        echo "\r\nGerando assets na versao -> ".$version;
        
        echo "\r\nCriando o diretorio public";
        exec('mkdir -p public/assets/js');
        exec('mkdir -p public/assets/css');
        exec('mkdir -p public/assets/fonts');

        echo "\r\nuglify e unindo arquivos js";
        $dirs = array_filter(glob('resources/assets/js/*'), 'is_dir');

        foreach ($dirs as $dir){
            $name = explode("/",$dir)[sizeof(explode("/",$dir))-1];
            echo "\r\nCompilando -> ".$name."\r\n";
            echo exec('uglifyjs resources/assets/js/'.$name.'/*.js -c -o public/assets/js/'.$name.'-'.$version.'.min.js');
        }

        echo "\r\nCompilando react js";
        echo "\r\n";
        echo exec('npm install && webpack -p');

        echo "\r\nunindo e minificando arquivos css";
        $dirs = array_filter(glob('resources/assets/scss/*'), 'is_dir');

        foreach ($dirs as $dir){
            $name = explode("/",$dir)[sizeof(explode("/",$dir))-1];
            echo "Compilando -> ".$name."\r\n";
            echo exec('node-sass --output-style compressed resources/assets/scss/'.$name.'/main.scss > public/assets/css/'.$name.'-'.$version.'.min.css');
        }
        echo "\r\ncopiando arquivos de fonts";
        exec('cp resources/assets/fonts/*.* public/assets/fonts/');

        echo "\r\nOtimizando imagens.....";
        $dirs = array_filter(glob('resources/assets/img/*'), 'is_dir');

        foreach ($dirs as $dir){
            $name = explode("/",$dir)[sizeof(explode("/",$dir))-1];
					  $filesPNG = glob('resources/assets/img/'.$name.'/*.png', GLOB_BRACE);
					  $filesGIF = glob('resources/assets/img/'.$name.'/*.gif', GLOB_BRACE);
            echo "\n\rOtimizando pasta -> ".$dir."\r\n";
            echo "\n\r------------> Otimizando JPGS <--------------\r\n";

		        exec('mkdir -p public/assets/img/'.$name);
					
						echo exec('jpegoptim -f -m 80 -t -o '.$dir.'/*.jpg --dest=public/assets/img/'.$name.'/');

            echo "\n\r------------> Otimizando PNGS <--------------\r\n";
					
						foreach($filesPNG as $arquivo) {
							$nomeArquivo = explode("/",$arquivo);
							$nomeArquivo = $nomeArquivo[sizeof($nomeArquivo)-1];
							
							echo "\n\rOtimizando arquivo -> ".$arquivo."\n\r";
							if (file_exists('public/assets/img/'.$name.'/'.$nomeArquivo))
								echo "ARQUIVO JA EXISTE MANTENDO VERSAO ATUAL....";
							else
								echo exec('optipng -force -o5 '.$arquivo.' -out public/assets/img/'.$name.'/'.$nomeArquivo);

						}

						foreach($filesGIF as $arquivo) {
							$nomeArquivo = explode("/",$arquivo);
							$nomeArquivo = $nomeArquivo[sizeof($nomeArquivo)-1];
							echo "\n\rCopiando GIFS -> ".$arquivo."\n\r";
							echo exec('cp '.$arquivo.' public/assets/img/'.$name.'/'.$nomeArquivo);
						}

        }

        echo "\r\nCompila��o de assets completa";

				if ($this->option('watch')){
					echo "\r\nWATCH ATIVADO ESTE COMANDO VAI RODAR SEMPRE Q ALGO FOR MODIFICADO NESTA PASTA";
					echo exec('watch -p "resources/assets/**/*(*.scss|*.js|*.jpg|*.gif|*.png)"  -c "php artisan assets:publicar"');
				}

    }
}
