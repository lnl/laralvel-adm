<?php

namespace Uhuu\Adm;

use Illuminate\Console\Command;

class Git extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'git:push {mensagem}';

    /**
     * The console command description.
     *ra
     * @var string
     */
    protected $description = 'Realiza pull add commit e push do git';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        exec("cd ".base_path());
        print_r(exec("git add -A"));
        exec("git commit -m \"".$this->argument('mensagem')."\"");
        print_r(exec("git pull"));
        print_r(exec("git push"));


    }
}
